# REST description

Path prefix: http://localhost:8080/api/
    - path example: http://localhost:8080/api/books/all


Following api description sructure:

## API Part (path midle)
- **METHOD**
    - Path postfix
        - Additional info (header, body example request/response, ...)

## books/
- **GET**
    - all
        - request authorization: "bearer": [{
        		"key": "token",
        		"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBobC5jeiIsImV4cCI6MTYxMDU3ODgwMH0.970iaTa5vlfKO2NqWq3WE1jI_2O8xRMBXvDr-eh8oORzRSgLIufNYmVl_15eJZX1caF_pnwSEE4D97_Cy_KULA",
        		"type": "string"
        	}]
    - available
        - request authorization: YET UNKNOWN
    - borrowed
        - request authorization: YET UNKNOWN
    - {id}
        - request authorization: YET UNKNOWN
    - library/{library_id}
        - request authorization: YET UNKNOWN
    - library/available/{library_id}
        - request authorization: YET UNKNOWN
    - library/borrowed/{library_id}
        - request authorization: YET UNKNOWN
    - tag/{tag_text}
        - request authorization: "bearer": [{
        		"key": "token",
        		"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBobC5jeiIsImV4cCI6MTYxMDU3ODgwMH0.970iaTa5vlfKO2NqWq3WE1jI_2O8xRMBXvDr-eh8oORzRSgLIufNYmVl_15eJZX1caF_pnwSEE4D97_Cy_KULA",
        		"type": "string"
        	}]
- **POST**
    - create
        - request authorization: "bearer": [{
        		"key": "token",
        		"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyMkBobC5jeiIsImV4cCI6MTYxMDU3ODgwMH0.os3falg6LGtN2cHoTF7Qm7HS8lZ2sQv64ctx-erguWQStIHsy7wsZZ9uu08grmUAyGdddtxERWKNVEJ8WUZ6ew",
        		"type": "string"
        	}]
        - request body: {
    		"mode": "raw",
    		"raw": "{\r\n    \"title\": \"A\",\r\n    \"author\": \"ADSDFD\",\r\n    \"isbn\": \"123455\",\r\n    \"available\": false\r\n}",
    		"options": {
    			"raw": {
    				"language": "json"
    			}
    		}}
    - borrow/{id}
        - request authorization: "bearer": [{
				"key": "token",
				"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyMkBobC5jeiIsImV4cCI6MTYxMDU3ODgwMH0.os3falg6LGtN2cHoTF7Qm7HS8lZ2sQv64ctx-erguWQStIHsy7wsZZ9uu08grmUAyGdddtxERWKNVEJ8WUZ6ew",
				"type": "string"
            }]
    - return/{id}
        - request authorization: "bearer": [{
				"key": "token",
                "value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyMkBobC5jeiIsImV4cCI6MTYxMDU3ODgwMH0.os3falg6LGtN2cHoTF7Qm7HS8lZ2sQv64ctx-erguWQStIHsy7wsZZ9uu08grmUAyGdddtxERWKNVEJ8WUZ6ew",
				"type": "string"
            }]
    - add-tag/{book_id}
        - request authorization: "bearer": [{
				"key": "token",
                "value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBobC5jeiIsImV4cCI6MTYxMDU3ODgwMH0.970iaTa5vlfKO2NqWq3WE1jI_2O8xRMBXvDr-eh8oORzRSgLIufNYmVl_15eJZX1caF_pnwSEE4D97_Cy_KULA",
				"type": "string"
            }]
        - request body: {
			"mode": "raw",
			"raw": "{\r\n    \"text\": \"TAG3\"\r\n}",
			"options": {
				"raw": {
					"language": "json"
				}
			}}
    - remove-tag/{book_id}
        - request authorization: "bearer": [{
				"key": "token",
                "value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyMUBobC5jeiIsImV4cCI6MTYxMDU3ODgwMH0.w6XZ0A5irjh834m3DyJ19dnR0Qc7Aq0yDQlpVep_THdO8LW40uTan9wK3zbtWKsM2RpJ2MKLcWde9lku1095EQ",
				"type": "string"
            }]
        - request body: {
			"mode": "raw",
			"raw": "{\r\n    \"text\": \"TAG2\"\r\n}",
			"options": {
				"raw": {
					"language": "json"
				}
			}}
- **PUT**
    - update/{id}
        - request authorization: "bearer": [{
    			"key": "token",
    			"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyZXN0VXNlckByZXN0LmN6IiwiZXhwIjoxNjEwNDkyNDAwfQ.FMN6v2BmUQB7xAcqE0PAf2VNiPtdXMRcb7gApGlXupWkA1p5HRBdjMQ_q-GwmfKFFeKbk2mxu_YA0g_eZIjqVQ",
    			"type": "string"
            }]
        - request body: {
            "mode": "raw",
    		"raw": "{\r\n    \"title\": \"AA\",\r\n    \"author\": \"CCCC\",\r\n    \"isbn\": \"54321\",\r\n    \"available\": true,\r\n    \"availableFrom\": \"2020-11-30\"\r\n}",
    		"options": {
    			"raw": {
    				"language": "json"
    			}
    		}}
- **DELETE**
    - delete/{id}
        - request authorization: "bearer": [{
    			"key": "token",
    			"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBobC5jeiIsImV4cCI6MTYxMDU3ODgwMH0.970iaTa5vlfKO2NqWq3WE1jI_2O8xRMBXvDr-eh8oORzRSgLIufNYmVl_15eJZX1caF_pnwSEE4D97_Cy_KULA",
    			"type": "string"
            }]

## library/
- **GET**
    - all
        - request authorization: "noauth"
- **PUT**
    - update/{id}
        - request authorization: "bearer": [{
				"key": "token",
				"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBobC5jeiIsImV4cCI6MTYxMDU3ODgwMH0.970iaTa5vlfKO2NqWq3WE1jI_2O8xRMBXvDr-eh8oORzRSgLIufNYmVl_15eJZX1caF_pnwSEE4D97_Cy_KULA",
				"type": "string"
			}]
- **DELETE**
    - delete/{id}
        - request authorization: "bearer": [{
				"key": "token",
				"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyMUBobC5jeiIsImV4cCI6MTYxMDU3ODgwMH0.w6XZ0A5irjh834m3DyJ19dnR0Qc7Aq0yDQlpVep_THdO8LW40uTan9wK3zbtWKsM2RpJ2MKLcWde9lku1095EQ",
				"type": "string"
			}]

## auth/	-- login & user schemes
- **POST**
    - auth
        - request authorization: YET UNKNOWN
        - request body: {
            "mode": "raw",
    		"raw": "{\r\n    \"email\": \"user1@hl.cz\",\r\n    \"password\": \"123\"\r\n}",
    		"options": {
    			"raw": {
    				"language": "json"
    			}
    		}}

## tag/
- **GET**
    - {id}
        - request authorization: YET UNKNOWN

## user/
- **GET**
    - current
        - request authorization: "bearer": [{
				"key": "token",
				"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyMkBobC5jeiIsImV4cCI6MTYxMDU3ODgwMH0.os3falg6LGtN2cHoTF7Qm7HS8lZ2sQv64ctx-erguWQStIHsy7wsZZ9uu08grmUAyGdddtxERWKNVEJ8WUZ6ew",
				"type": "string"
			}]
	- {id}
        - request authorization: "bearer": [{
				"key": "token",
				"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBobC5jeiIsImV4cCI6MTYxMDQ5MjQwMH0.uAP3ctOl4VtQI2w78lplruPaXGxr2bMIE7Li4mxUHV8iFJWNJLSyF0BIn8uM7Nxk2_i2HvfnesfjcQbDY1PqMA",
				"type": "string"
			}]
	- all
        - request authorization: "bearer": [{
				"key": "token",
				"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyZXN0VXNlckByZXN0LmN6IiwiZXhwIjoxNjEwNDkyNDAwfQ.FMN6v2BmUQB7xAcqE0PAf2VNiPtdXMRcb7gApGlXupWkA1p5HRBdjMQ_q-GwmfKFFeKbk2mxu_YA0g_eZIjqVQ",
				"type": "string"
			}]
- **POST**
    - register
        - request authorization: YET UNKNOWN
        - request body: {
            "mode": "raw",
    		"raw": "{\r\n    \"firstname\": \"restUser\",\r\n    "surname": "UserRest",\r\n    "email": "restUser@rest.cz",\r\n    "password": "123"\r\n}",
    		"options": {
    			"raw": {
    				"language": "json"
    			}
    		}}
	- register-admin
        - request authorization: "bearer": [{
				"key": "token",
				"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBobC5jeiIsImV4cCI6MTYxMDU3ODgwMH0.970iaTa5vlfKO2NqWq3WE1jI_2O8xRMBXvDr-eh8oORzRSgLIufNYmVl_15eJZX1caF_pnwSEE4D97_Cy_KULA",
				"type": "string"
			}]
        - request body: {
            "mode": "raw",
    		"raw": "{\r\n    \"firstname\": \"restAdmin\",\r\n    "surname": "AdminRest",\r\n    "email": "restAdmin@rest.cz",\r\n    "password": "123"\r\n}",
    		"options": {
    			"raw": {
    				"language": "json"
    			}
    		}}
	- create-library
        - request authorization: "bearer": [{
				"key": "token",
				"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyZXN0VXNlckByZXN0LmN6IiwiZXhwIjoxNjEwNDkyNDAwfQ.FMN6v2BmUQB7xAcqE0PAf2VNiPtdXMRcb7gApGlXupWkA1p5HRBdjMQ_q-GwmfKFFeKbk2mxu_YA0g_eZIjqVQ",
				"type": "string"
			}]


## Request example "api/user/register-admin":
    "auth": {
		"type": "bearer",
		"bearer": [
			{
				"key": "token",
				"value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBobC5jeiIsImV4cCI6MTYxMDU3ODgwMH0.970iaTa5vlfKO2NqWq3WE1jI_2O8xRMBXvDr-eh8oORzRSgLIufNYmVl_15eJZX1caF_pnwSEE4D97_Cy_KULA",
				"type": "string"
			}
		]
	},
	"method": "POST",
	"header": [],
	"body": {
		"mode": "raw",
		"raw": "{\r\n    \"firstName\": \"restAdmin\",\r\n    \"surname\": \"AdminRest\",\r\n    \"email\": \"restAdmin@rest.cz\",\r\n    \"password\": \"123\"\r\n}",
		"options": {
			"raw": {
				"language": "json"
			}
		}
	},
	"url": {
		"raw": "http://localhost:8080/api/user/register-admin",
		"protocol": "http",
		"host": [
			"localhost"
		],
		"port": "8080",
		"path": [
			"api",
			"user",
			"register-admin"
		]
	}