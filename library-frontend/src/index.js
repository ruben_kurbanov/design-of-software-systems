import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import App from './App';

//var address =  "https://domaci-knihovna.herokuapp.com/"
var address = "http://localhost:8080/"
// defaulting to localhost in case if script invoked not via "npm run-script" but directly
//var address = process.env.npm_config_backendAddress || "http://localhost:8080/"

ReactDOM.render(
  <React.StrictMode>
    <App address={address}/>
  </React.StrictMode>,
  document.getElementById('root')
);
