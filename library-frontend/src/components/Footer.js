import React from 'react'
import { Link } from 'react-router-dom'

class Footer extends React.Component {
    render() {
        return (
            <footer>
                <p>
                <Link to="/">Home</Link>
                |
                <Link to="/setting">Settings</Link>
                |
                <Link to="/">FAQ</Link>
                |
                <Link to="/">Company</Link>
                |
                <Link to="/">Kontakt</Link>
                </p>
            </footer>

        )
    }
}

export default Footer