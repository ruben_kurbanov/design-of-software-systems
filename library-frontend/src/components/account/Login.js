import React from 'react';
import sha256 from 'js-sha256'
import { Link, withRouter } from 'react-router-dom';

import '../../css/account/LoginRegistration.css';

class Login extends React.Component {
    response = {};
    responseStatus = 0;
    responseToken = "";
    bodyReader = null

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            email: "",
            password: "",
            wrongEmail: false,
            wrongPassword: false,
        };
    }

    componentDidMount() {
        this.setState({
            loaded: true
        });
    }

    setEmail(text) {
        this.setState({
            email: text
        });
    }

    setPassword(text) {
        this.setState({
            password: text
        });
    }

    hashPassword() {
        return sha256(this.state.password.toString());
    }

    async loginUser() {
        return fetch(this.props.addressBE + 'api/auth', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                /*'Authorization': 'Bearer ' + this.props.token*/
            },
            body: JSON.stringify({
                "email": this.state.email,
                "password": this.hashPassword()
            })
        }).then(
            result => {
                this.response = result;
                this.responseStatus = result.status;

                result.json().then(body => {
                    this.responseToken = body.token;
                    if (this.responseToken != null) {
                        this.props.setToken(this.responseToken);
                    }
                });
            },
            error => {
                console.err(error);
            }
        );
    }

    handleSubmit = async e => {
        e.preventDefault();
        try {
            await this.loginUser();
        } catch (err) {
            console.log(this.response);
            console.error(err);
            window.alert("We are sorry, server is unreachable.\nCommunicatiuon error.");
            return;
        }
        if (this.responseStatus >= 200 && this.responseStatus < 300) {
            if (this.responseToken != null) {
                this.props.history.push("/");
            } else {
                window.alert("Unable to log in. Please contact the authorities.")
            }
        } else if (this.responseStatus >= 400 && this.responseStatus < 500) {
            if (this.responseStatus == 403) {
                this.setState({
                    wrongEmail: false,
                    wrongPassword: true
                });
            } else if (this.responseStatus == 404) {
                this.setState({
                    wrongEmail: true,
                    wrongPassword: false
                });
            } else  {
                this.setState({
                    wrongEmail: true,
                    wrongPassword: true
                });
            }
            this.render();
        } else {
            window.alert("We are sorry, server is unreachable.")
        }
    }

    render() {
        return (
            <main>
                <div className="main-div">
                    <h1 className="head-login-registration">
                        <Link className="link-login" to="/login" state="selected">
                            Přihlášení
                        </Link>
                        |
                        <Link className="link-registration" to="/registration" state="unselected">
                            Registrace
                        </Link>
                    </h1>
                    <form onSubmit={this.handleSubmit}>
                        <label>
                            <p>E-mail</p>
                            <p className="error-message"  hidden={!this.state.wrongEmail}>
                                ! Uživatel s touto e-mailovou adresou není v databázi !
                            </p>
                            <input className="email" type="text" placeholder="priklad@mail.cz"
                                value={this.state.email} onChange={e => this.setEmail(e.target.value)}/>
                        </label>
                        <label>
                            <p>Heslo</p>
                            <p className="error-message"  hidden={!this.state.wrongPassword}>
                                ! Nesprávné heslo !
                            </p>
                            <input className="password" type="password" placeholder="******"
                                onChange={e => this.setPassword(e.target.value)}/>
                        </label>
                        <div className="div-button">
                            <button className="login-button" type="submit">Přihlásit se</button>
                        </div>
                    </form>
                </div>
            </main>
        );
    }
}

export default withRouter(Login)