import React from 'react';
import '../../css/account/Setting.css';

export default class Setting extends React.Component {

    
    constructor(props) {
        super(props);
        this.state = {
            loaded: false
        }
    }

    componentDidMount() {
        this.setState({
            loaded: true
        })
    }

    render() {
        return (
            <main>
                <div className="main-div">
                    <h1 className="head-login_settings">
                        <a className="link_setings">Settings</a>
                    </h1>

                    <form>
                        <fieldset className = "field">
                            <legend>Settings</legend>
                            <h1>Personal</h1>

                            <p>Name:
                                <a class = "name">NAME</a>
                            </p>

                            <p>Enter your new name
                                <input className="name_change" type="text"></input>
                            </p>

                            <hr></hr>
                            <p>Surname: <a class = "surname">SURNAME</a></p>
                                <p>Enter your new surname  <input className="surname_change" type="text"></input></p>

                            <hr></hr>
                            <p>Email:
                                <a class = "email">EMAIL</a>
                            </p>
                            <p>Enter your new email
                                <input className="email_change" type="text"></input>
                            </p>

                            <hr></hr>
                            <p>Password:
                                <a class = "password_chaged">ONE YEAR AGO</a>
                            </p>
                            <p>Enter your new password
                                <input className="p1_change" type="text"></input>
                            </p>
                            <p>Enter your password again
                                <input className="p1_change" type="text"></input>
                            </p>
                            <h1>Design</h1>

                            <ol>
                                <li>
                                    <input type="checkbox" name="design" value="night" ></input>Night mode
                                </li>
                                <li>
                                    <input type="checkbox" name="design" value="blind" ></input>Mode for the visually impaired
                                </li>
                            </ol>
                        </fieldset>
                    </form>

                    <button className="submit" type="submit">Done</button>

                </div>
            </main>
        )
    }
}