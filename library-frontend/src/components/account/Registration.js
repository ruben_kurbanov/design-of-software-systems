import React from 'react';
import sha256 from 'js-sha256';
import { Link, withRouter } from 'react-router-dom';

import '../../css/account/LoginRegistration.css';

const emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class Registration extends React.Component {
    response = {}
    responseStatus = 0;
    responseToken = "";
    bodyReader = null

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            firstName: "",
            surname: "",
            email: "",
            pass1: "",
            pass2: "",
            tooShortPassword: true,
            differentPasswords: true,
            wrongEmailFormat: true,
            unableToSubmit: true,
            existingMail: false,
        };
    }

    componentDidMount() {
        this.setState({
            loaded: true
        });
    }

    setFirstName(text) {
        this.setState({
            firstName: text
        });
        setTimeout(this.checkSubmitAbility, 200);
    }

    setSurname(text) {
        this.setState({
            surname: text
        });
        setTimeout(this.checkSubmitAbility, 200);
    }

    setEmail(text) {
        if (text != this.state.lastMail) {
            this.setState({
                email: text,
                existingMail: false,
            });
        } else {
            this.setState({
                email: text,
                existingMail: true,
            });
        }
        if (emailPattern.test(text)) {
            this.setState({
                wrongEmailFormat: false
            });
        } else {
            this.setState({
                wrongEmailFormat: true
            });
        }
        setTimeout(this.checkSubmitAbility, 200);
    }

    setPass1(text) {
        this.setState({
            pass1: text
        });
        if (text.length > 5) {
            this.setState({
                tooShortPassword: false
            });
        } else {
            this.setState({
                tooShortPassword: true
            });
        }
        setTimeout(this.checkPassEquility, 200);
        setTimeout(this.checkSubmitAbility, 200);
    }

    setPass2(text) {
        this.setState({
            pass2: text
        });
        setTimeout(this.checkPassEquility, 200);
        setTimeout(this.checkSubmitAbility, 200);
    }

    checkPassEquility = async e => {
        if (this.state.pass1 == this.state.pass2) {
            this.setState({
                differentPasswords: false
            });
        } else {
            this.setState({
                differentPasswords: true
            });
        }
    }

    checkSubmitAbility = async e => {
        if (!this.state.tooShortPassword && !this.state.differentPasswords && !this.state.wrongEmailFormat && !this.state.existingMail) {
            this.setState({
                unableToSubmit: false
            });
        } else {
            this.setState({
                unableToSubmit: true
            });
        }
        this.render();
    }

    hashPassword() {
        return sha256(this.state.pass1.toString());
    }

    async registerUser() {
        return fetch(this.props.addressBE + 'api/user/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "firstName": this.state.firstName,
                "surname": this.state.surname,
                "email": this.state.email,
                "password": this.hashPassword()
            })
        }).then(
            result => {
                this.response = result;
                this.responseStatus = result.status;

                result.json().then(body => {
                    this.responseToken = body.token;
                    if (this.responseToken != null) {
                        this.props.setToken(this.responseToken);
                    }
                });
            },
            error => {
                console.err(error);
            }
        );
    }

    handleSubmit = async e => {
        e.preventDefault();
        try {
            await this.registerUser();
        } catch (err) {
            console.log(this.response);
            console.error(err);
            window.alert("We are sorry, server is unreachable.\nCommunicatiuon error.");
            return;
        }
        if (this.responseStatus >= 200 && this.responseStatus < 300) {
            if (this.responseToken != null) {
                this.props.history.push("/");
            } else {
                window.alert("Unable to log in. Please contact the authorities.")
            }
        } else if (this.responseStatus >= 400 && this.responseStatus < 500) {
            if (this.responseStatus == 403) {
                this.setState({
                    existingMail: true,
                    lastMail: this.state.email,
                    unableToSubmit: true,
                });
            } else  {
                this.setState({
                    unableToSubmit: true,
                });
                window.alert("It seems like these credetials are already taken. Please try different values.")
            }
            this.render();
        } else {
            window.alert("We are sorry, server is unreachable.")
        }
    }

    render() {
        return (
            <main>
                <div className="main-div">
                    <h1 className="head-login-registration">
                        <Link className="link-login" to="/login" state="unselected">Přihlášení</Link>
                        |
                        <Link className="link-registration" to="/registration" state="selected">Registrace</Link>
                    </h1>
                    <form className="registration-form" onSubmit={this.handleSubmit}>
                        <label className="label-firstname">
                            <p>Křestní jméno</p>
                            <input className="firstname" type="text" placeholder="Jméno"
                                value={this.state.firstName} onChange={e => this.setFirstName(e.target.value)}/>
                        </label>
                        <label className="label-surname">
                            <p>Příjmení</p>
                            <input className="surname" type="text" placeholder="Příjmení"
                                value={this.state.surname} onChange={e => this.setSurname(e.target.value)}/>
                        </label>
                        <label className="label-email">
                            <p>E-mail</p>
                            <p className="error-message"  hidden={!this.state.wrongEmailFormat}>! Toto není validní formát e-mailové adresy !</p>
                            <p className="error-message"  hidden={!this.state.existingMail}>! Tento e-mail je již použit !</p>
                            <input className="email" type="text" placeholder="priklad@mail.cz"
                                value={this.state.email} onChange={e => this.setEmail(e.target.value)}/>
                        </label>
                        <label className="label-password1">
                            <p>Heslo</p>
                            <p className="error-message"  hidden={!this.state.tooShortPassword}>! Heslo je příliš krátké (minimum je 6 znaků) !</p>
                            <input className="password1" type="password" placeholder="******"
                                onChange={e => this.setPass1(e.target.value)}/>
                        </label>
                        <label className="label-password2">
                            <p>Opětovné heslo</p>
                            <p className="error-message" hidden={!this.state.differentPasswords}>! Hesla se neshodují !</p>
                            <input className="password2" type="password" placeholder="******"
                                onChange={e => this.setPass2(e.target.value)}/>
                        </label>
                        <div className="div-button">
                            <button className="register-button" type="submit" hidden={this.state.unableToSubmit}>
                                Zaregistrovat
                            </button>
                        </div>
                    </form>
                </div>
            </main>
        );
    }
}

export default withRouter(Registration)