import React from 'react';
import '../../css/account/Chat.css';

export default class Chat extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded: false
        };
        this.state = {value: ''};

        this.state = {list: []};

        this.handleChange = this.handleChange.bind(this);
        this.onClick = this.onClick.bind(this);

    }

    componentDidMount() {
        this.setState({
            loaded: true
        })
    }

    onClick(e) {
        alert(this.state.value);
        e.preventDefault();
        this.setState({value: ''});
    }


    handleChange(event) {
      this.setState({value: event.target.value});
    }


    render() {
        return (
             <main>
                <div className="main-div">
                    <h1 className="head-login_chat">
                    </h1>

                    <field className = "form_konverzace">
                       <fieldset className = "field_konverzace">
                            <legend>Konverzace</legend>
                            <body className = "body_k">
                                <ol className = "ol_konverzace">
                                    <li>
                                        <fieldset>
                                             <a className = "name_person">Name User1: </a>
                                             <a className = "mess_person">Message1</a>
                                             <img className = "image_person" src="https://i08.fotocdn.net/s128/f8db53b963f3d5f6/user_l/2913547370.jpg"/>
                                        </fieldset>
                                    </li>

                                    <li>
                                        <fieldset>
                                             <a className = "name_person">Name User1: </a>
                                             <a className = "mess_person">Message1</a>
                                             <img className = "image_person" src="https://i08.fotocdn.net/s128/f8db53b963f3d5f6/user_l/2913547370.jpg"/>
                                        </fieldset>
                                    </li>

                                    <li>
                                        <fieldset>
                                             <a className = "name_person">Name User1: </a>
                                             <a className = "mess_person">Message1</a>
                                             <img className = "image_person" src="https://i08.fotocdn.net/s128/f8db53b963f3d5f6/user_l/2913547370.jpg"/>
                                        </fieldset>
                                    </li>

                                    <li>
                                        <fieldset>
                                             <a className = "name_person">Name User1: </a>
                                             <a className = "mess_person">Message1</a>
                                             <img className = "image_person" src="https://i08.fotocdn.net/s128/f8db53b963f3d5f6/user_l/2913547370.jpg"/>
                                        </fieldset>
                                    </li>

                                    <li>
                                        <fieldset>
                                             <a className = "name_person">Name User1: </a>
                                             <a className = "mess_person">Message1</a>
                                             <img className = "image_person" src="https://i08.fotocdn.net/s128/f8db53b963f3d5f6/user_l/2913547370.jpg"/>
                                        </fieldset>
                                    </li>

                                    <li>
                                        <fieldset>
                                             <a className = "name_person">Name User1: </a>
                                             <a className = "mess_person">Message1</a>
                                             <img className = "image_person" src="https://i08.fotocdn.net/s128/f8db53b963f3d5f6/user_l/2913547370.jpg"/>
                                        </fieldset>
                                    </li>
                                </ol>
                            </body>

                       </fieldset>
                    </field>

                    <field className = "form_mess">
                        <fieldset className = "field_mess">
                            <legend>Message</legend>
                                <fieldset className = "person_select">
                                    <img className = "image_person" src="https://i08.fotocdn.net/s128/f8db53b963f3d5f6/user_l/2913547370.jpg"/>
                                    <a className = "name_select_person">Name User1: </a>
                                </fieldset>

                                <fieldset className = "messages_area">
                                    <body>
                                        <ol className="m">

                                            <li className = "get_mes">
                                                <fieldset className>
                                                    <a className = "message_me"><a className = "messageFrom">NamePerson: </a>Hello!</a>
                                                </fieldset>
                                            </li>

                                            <li className = "send_mes">
                                                <fieldset>
                                                    <a className = "message_you"><a>Your name: </a>Hi</a>
                                                </fieldset>
                                            </li>

                                        </ol>
                                    </body>

                                </fieldset>

                                <fieldset className = "massage_set_field">
                                    <textarea className = "text_area_mess" value={this.state.value} onChange={this.handleChange}></textarea>
                                </fieldset>

                                <div className >
                                    <button className="send" onClick = {this.onClick} type="submit">
                                        Send
                                    </button>
                                </div>

                        </fieldset>
                    </field>


                </div>
            </main>

        )
    }

}