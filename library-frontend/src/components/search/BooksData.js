const booksData = [
    {
        id: 1,
        imageSource: "https://www.knihydobrovsky.cz/thumbs/book-detail/mod_eshop/produkty/p/9788073903558_povidani-o-pejskovi-a-kocicce.5369.jpg",
        title: "Povídání o pejskovi a kočičce",
        author: "Josef Čapek",
        year: 2017,
        owner: "josef123"
    },
    {
        id: 2,
        imageSource: "https://www.levneknihy.cz/Document/156/156899/156899.jpg",
        title: "Bílá nemoc",
        author: "Karel Čapek",
        year: 2018,
        owner: "karel123"
    },
    {
        id: 3,
        imageSource: "https://www.levneknihy.cz/Document/144/144687/144687.jpg",
        title: "Povídky malostranské",
        author: "Jan Neruda",
        year: 2017,
        owner: "sebekjecool"
    }
];

export default booksData;