import React from 'react'
import './../../css/search/search.css'
import booksData from "./BooksData";
import SearchResult from "./SearchResult";

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            data: booksData,
            searchingBooks: true,
            name: "",
            error: null
        }
        this.handleInputChange.bind(this)
        this.handleSubmit.bind(this)
    }

    componentDidMount() {
        var suffix = "all";
        if (this.props.token === "") {
            suffix = "available";
        }
        fetch(this.props.addressBE + "api/books/" + suffix, {
            method: 'GET',
            headers: {
                /*'Authorization': 'Bearer ' + this.props.token*/
            }
        }).then(
            result => {
                console.log(result);
                result.json().then(body => {
                    console.log(body);
                    this.setState({
                        isLoaded: true,
                        data: body
                    })
                });
            },
            error => {
                console.log(error);
            }
        );
    }

    handleInputChange = event => {
        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        const name = event.target.name;
        this.setState({
            name: value
        });
        event.preventDefault();
    }

    handleSubmit = event => {
        console.log();
        event.preventDefault();
                                // + "api/books/tag/" + this.state.name
        fetch(this.props.addressBE + "api/books/all", {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + this.props.token
            }
        }).then(
            result => {
                console.log(result);
                result.json().then(body => {
                    console.log(body);
                    this.setState({
                        isLoaded: true,
                        data: body
                    })
                });
            },
            error => {
                this.setState({
                    isLoaded: true,
                    error
                })
            }
        );
    }

    render() {
        
        const searchResults = this.state.data.map(result =>
            <SearchResult
                key={result.id}
                resultType={"book-result"}
                imageSource={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS79R7oxwgo42x6shlatYBY8dgZbgP_pP866UlRMKHAJFDq7TxMIRZL7FUlfCLfi5drGEY&usqp=CAU"}
                        //   result.imageSource
                title={result.title}
                author={result.author}
                year={result.year}
                owner={result.owner}
            />
        );

        
        const {error, isLoaded, data} = this.state;
        if (error) {
            return (
                <main>
                    <p>Error: {error}</p>
                </main>
            )
        } else if (!isLoaded) {
            return (
                <main>
                    <p>Loading...</p>
                </main>
            )
        } else {
            return (
                <main>
                    <div className={"search-bar"}>
                        <p>Hledám:</p>
                        <form className={"search-form"} onSubmit={this.handleSubmit}>
                            <div className={"search-buttons"}>
                                <label>
                                    <input
                                        type={"radio"}
                                        name={"searchType"}
                                        value={"books"}
                                        className={"search-radio"}
                                        id={"search-book-button"}
                                        checked={this.state.searchingBooks}
                                    />
                                    Knihu
                                </label>
                                <label>
                                    <input
                                        type={"radio"}
                                        name={"searchType"}
                                        value={"users"}
                                        className={"search-radio"}
                                        id={"search-user-button"}
                                        checked={!this.state.searchingBooks}
                                    />
                                    Uživatele
                                </label>
                            </div>
                            <input
                                type={"text"}
                                name={"textInput"}
                                id={"search-input"}
                            />
                            <input
                                type={"submit"}
                                value={"Vyhledat"}
                                id={"search-button"}
                            />
                        </form>
                    </div>
                    <fieldset>
                        <legend>Výsledky hledání</legend>
                        {searchResults}
                    </fieldset>
                </main>
            )
        }
    }
}

export default Search;