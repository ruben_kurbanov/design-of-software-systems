import React from 'react'
import { Link } from 'react-router-dom';

class Header extends React.Component {

    constructor(props) {
        super(props);
    }

    logout = async e => {
        this.props.resetToken();
    }

    render() {
        return (
            <header>
                <Link to="/">
                    <div className="header-link_home">
                        <img src={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS79R7oxwgo42x6shlatYBY8dgZbgP_pP866UlRMKHAJFDq7TxMIRZL7FUlfCLfi5drGEY&usqp=CAU"} alt={""} />
                        <h1>Domácí knihovna</h1>
                    </div>
                </Link>
                <p className='header-login_registration'>
                    <Link className="header-link_login" to="/login" hidden={!this.props.userLoggedOut}>
                        Přihlásit se/
                    </Link>
                    <Link className="header-link_register" to="/registration" hidden={!this.props.userLoggedOut}>
                        Registrovat
                    </Link>
                    <Link className="header-link_logout" to="/" hidden={this.props.userLoggedOut} onClick={this.logout}>
                        Odhlásit se
                    </Link>
                </p>
            </header>
        )

    }
}

export default Header