import React from "react";

import BookData from "./utils/BookData";

import "../../css/library/Base.css"
import "../../css/library/My.css"
import LibraryData from "./utils/LibraryData";
import getBookHTML from "./utils/utils";
import UserData from "./utils/UserData";


export default class My extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
        }
        this.addBookLink = "";
        // TODO: {important} change token
        this.token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyMUBobC5jeiIsImV4cCI6MTYyMjA2NjQwMH0.TiU4WCYJM7ed27Rv2l6qniEyewbVbU0AyCFyJERcW3zp7dIYn1iD7G8fQzilW0wEDLiD-ZbYw0nGSHaiUSrc1w";
    }

    componentDidMount() {
        this.setState({
            loaded: true
        })

        this.loadData();
    }

    async loadData() {
        const userData = await this.loadCurrentUserData();
        if (userData.role !== "USER") {
            return;
        }

        this.displayMyLibrary();
        this.displayBorrowedBooks();
        this.displayReservedBooks();
    }

    async displayMyLibrary() {
        const data = await this.loadMyLibrary();

        let books = [];
        for (let i = 0; i < data.books.length; i++) {
            const book = new BookData("myBook", data.books[i].id, data.books[i].author, data.books[i].title, data.books[i].isbn, data.books[i].available, data.books[i].availableFrom, "")
            books.push(book);
        }
        const libraryData = new LibraryData(data.id, data.borrowingPeriod, data.visible, books);

        let myLibraryHTML = "";
        for (let i = 0; i < libraryData.books.length; i++) {
            myLibraryHTML += getBookHTML(libraryData.books[i], true);
        }

        document.getElementById("my-books").innerHTML = myLibraryHTML;
    }

    async loadMyLibrary() {
        return fetch("http://localhost:8080/api/library/current", {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.token}`
            }
        })
            .then(response => response.json());
    }

    // TODO: need test
    async displayBorrowedBooks() {
        const data = await this.loadBorrowedBooks(0);

        let borrowedBooksHTML = "";
        for (let i = 0; i < data.length; i++) {
            const book = new BookData("borrowedBook", data[i].id, data[i].author, data[i].title, data[i].isbn, data[i].available, data[i].availableFrom, "");
            const loadedUserData = await this.loadUserDataByBookId(book.id);
            const userId = loadedUserData.id;
            book.links.ownerChat = "/chat/" + userId.toString();

            borrowedBooksHTML += getBookHTML(book, true);
        }

        document.getElementById("borrowed-books").innerHTML = borrowedBooksHTML;
    }

    // TODO: need implement
    async loadBorrowedBooks(userId = 0) {
        let data = []
        data.push(new BookData("borrowedBook", "1"));
        data.push(new BookData("borrowedBook", "2"));
        data.push(new BookData("borrowedBook", "3"));
        return data;


        // TODO: need server support; change url; uncomment
        // const url = "http://localhost:8080/api/books/borroewd/user/" + userId.toString();
        // return fetch(url, {
        //     method: 'GET',
        //     headers: {
        //         'Content-Type': 'application/json',
        //         Authorization: `Bearer ${this.token}`
        //     }
        // })
        //     .then(response => response.json());
    }

    // TODO: need test
    async displayReservedBooks() {
        const data = await this.loadReservedBooks(0);

        let reservedBooksHTML = "";
        for (let i = 0; i < data.length; i++) {
            const book = new BookData("reservedBook", data[i].id, data[i].author, data[i].title, data[i].isbn, data[i].available, data[i].availableFrom, "");
            const loadedUserData = await this.loadUserDataByBookId(book.id);
            const userId = loadedUserData.id;
            book.links.ownerChat = "/chat/" + userId.toString();

            reservedBooksHTML += getBookHTML(book, true);
        }

        document.getElementById("reserved-books").innerHTML = reservedBooksHTML;
    }

    // TODO: need implement
    async loadReservedBooks(userId = 0) {
        let data = []
        data.push(new BookData("reservedBook", "1"));
        data.push(new BookData("reservedBook", "2"));
        data.push(new BookData("reservedBook", "3"));
        data.push(new BookData("reservedBook", "4"));
        data.push(new BookData("reservedBook", "5"));
        return data;

        // TODO: need server support; change url; uncomment
        // const url = "http://localhost:8080/api/books/reserved/user/" + userId.toString();
        // return fetch(url, {
        //     method: 'GET',
        //     headers: {
        //         'Content-Type': 'application/json',
        //         Authorization: `Bearer ${this.token}`
        //     }
        // })
        //     .then(response => response.json());
    }

    // TODO: need implement
    async loadUserDataByBookId(userId = 0) {
        return new UserData();

        // TODO: need server support; change url; uncomment
        // const url = "http://localhost:8080/api/books/user/" + userId.toString();
        // return fetch(url, {
        //     method: 'GET',
        //     headers: {
        //         'Content-Type': 'application/json',
        //         Authorization: `Bearer ${this.token}`
        //     }
        // })
        //     .then(response => response.json());
    }

    async loadCurrentUserData() {
        return fetch("http://localhost:8080/api/user/current", {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.token}`
            }
        })
            .then(response => response.json());
    }

    render() {
        return (
            <main>
                <div className={"main-div"}>
                    <div className={"add-book-link-div"}>
                        <a className={"link-button"} href={this.addBookLink}>Přidat knihu</a>
                    </div>
                </div>
                <br/>
                <br/>
                <div className={"hbox space-around"}>
                    <div className={"scroll-widget scroll-widget-small"}>
                        <b className={"scroll-widget-header-text"}>Moje knihy</b>
                        <div id={"my-books"}>
                            {/* display: async displayMyLibrary */}
                        </div>
                    </div>
                    <div className={"scroll-widget scroll-widget-small"}>
                        <b className={"scroll-widget-header-text"}>Vypůjčené knihy</b>
                        <div id={"borrowed-books"}>
                            {/* display: async displayBorrowedBooks */}
                        </div>
                    </div>
                    <div className={"scroll-widget scroll-widget-small"}>
                        <b className={"scroll-widget-header-text"}>Rezervované knihy</b>
                        <div id={"reserved-books"}>
                            {/* display: async displayReservedBooks */}
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}