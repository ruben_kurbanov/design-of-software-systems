import React from "react";

import getBookHTML from "./utils/utils";

import UserData from "./utils/UserData";
import LibraryData from "./utils/LibraryData";
import BookData from "./utils/BookData";

import "./../../css/library/Foreign.css"
import "./../../css/library/Base.css"
import "./../../css/library/Book.css"

export default class Foreign extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userId: props.match.params.userId,
            loaded: false,
        }
        // TODO: {important} change token
        this.token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyMUBobC5jeiIsImV4cCI6MTYyMjA2NjQwMH0.TiU4WCYJM7ed27Rv2l6qniEyewbVbU0AyCFyJERcW3zp7dIYn1iD7G8fQzilW0wEDLiD-ZbYw0nGSHaiUSrc1w";
    }

    componentDidMount() {
        this.setState({
            loaded: true
        })

        this.loadData();
    }

    loadData() {
        this.displayOwnerData();
        this.displayOwnerLibrary();
    }

    async displayOwnerData() {
        const data = await this.loadOwnerData();

        const ownerData = new UserData(data.id, data.email, data.firstName, data.surname, data.role);
        ownerData.chatLink = "/chat/" + this.state.userId;

        let ownerHTMl = "";
        ownerHTMl +=
            "<div class='hbox space-between'>" +
            "   <div class='owner-image-div'>";
        if (ownerData.photoLink !== "") {
            ownerHTMl +=
                "   <img class='owner-image' src='" + ownerData.photoLink + "' alt='' />";
        } else {
            ownerHTMl +=
                "   <svg class='owner-image' xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 460.8 460.8' xml:space='preserve'><g><path d='M230.432,0c-65.829,0-119.641,53.812-119.641,119.641s53.812,119.641,119.641,119.641s119.641-53.812,119.641-119.641    S296.261,0,230.432,0z'/></g><g><path d='M435.755,334.89c-3.135-7.837-7.314-15.151-12.016-21.943c-24.033-35.527-61.126-59.037-102.922-64.784    c-5.224-0.522-10.971,0.522-15.151,3.657c-21.943,16.196-48.065,24.555-75.233,24.555s-53.29-8.359-75.233-24.555    c-4.18-3.135-9.927-4.702-15.151-3.657c-41.796,5.747-79.412,29.257-102.922,64.784c-4.702,6.792-8.882,14.629-12.016,21.943    c-1.567,3.135-1.045,6.792,0.522,9.927c4.18,7.314,9.404,14.629,14.106,20.898c7.314,9.927,15.151,18.808,24.033,27.167    c7.314,7.314,15.673,14.106,24.033,20.898c41.273,30.825,90.906,47.02,142.106,47.02s100.833-16.196,142.106-47.02    c8.359-6.269,16.718-13.584,24.033-20.898c8.359-8.359,16.718-17.241,24.033-27.167c5.224-6.792,9.927-13.584,14.106-20.898    C436.8,341.682,437.322,338.024,435.755,334.89z'/></g></svg>";
        }
        ownerHTMl +=
            "   </div>" +
            "   <div class='vbox space-around text-align-center'>" +
            "       <span>" + ownerData.firstName + " " + ownerData.surname + "</span>" +
            "       <span>" + ownerData.email + "</span>" +
            "   </div>" +
            "   <div class='owner-link-div'>\n" +
            "       <a class='link-line' href='" + ownerData.chatLink+ "'>Napsat uživateli</a>" +
            "   </div>" +
            "</div>"

        document.getElementById("owner-div").innerHTML = ownerHTMl;
    }

    async loadOwnerData() {
        const url = "http://localhost:8080/api/user/" + this.state.userId;
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.token}`
            }
        })
            .then(response => response.json());
    }

    async displayOwnerLibrary() {
        const data = await this.loadOwnerLibrary()

        console.log(data);

        let books = [];
        for (let i = 0; i < data.books.length; i++) {
            const book = new BookData("foreignBook", data.books[i].id, data.books[i].author, data.books[i].title, data.books[i].isbn, data.books[i].available, data.books[i].availableFrom, "")
            books.push(book);
        }
        const libraryData = new LibraryData(data.id, data.borrowingPeriod, data.visible, books);

        console.log(libraryData);

        let ownerLibraryHTML = "";
        for (let i = 0; i < libraryData.books.length; i++) {
            ownerLibraryHTML +=
                "<div class='book-place'" +
                    getBookHTML(libraryData.books[i]) +
                "</div>"
        }

        document.getElementById("owner-library").innerHTML = ownerLibraryHTML;
    }

    async loadOwnerLibrary() {
        const url = "http://localhost:8080/api/library/user/" + this.state.userId;
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.token}`
            }
        })
            .then(response => response.json());
    }

    render() {
        return (
            <main>
                <div className={"main-div"}>
                    <div id={"owner-div"} className={"owner-div"}>
                        {/* display: async displayOwnerData() */}
                    </div>
                    <div className={"scroll-widget scroll-widget-big"}>
                        <b className={"scroll-widget-header-text"}>Knihy uživatele</b>
                        <div id={"owner-library"} className={"wrap-box"}>
                            {/* display: async displayOwnerLibrary */}
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}