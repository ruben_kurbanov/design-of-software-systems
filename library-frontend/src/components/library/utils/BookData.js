export default class BookData {

    // type: myBook|borrowedBook|reservedBook|foreignBook
    constructor(
        type = "myBook",
        id = "id",
        author = "author",
        title = "title",
        isbn = "isbn",
        available = "available",
        availableFrom = "availableFrom",
        imageLink = "")
    {
        this.id = id;
        this.author = author;
        this.title = title;
        this.isbn = isbn;
        this.available = available;
        this.availableFrom = availableFrom;
        this.imageLink = imageLink;
        // TODO undefined value
        this.yearOfPublication = "1970";
        if (this.available === true) {
            this.bookState = "Dostupná"
        } else {
            this.bookState = "Vypůjčená"
        }
        this.links = {
            type: type,
            editBook: "",
            deleteBook: "",
            changeBookState: "",
            returnBook: "",
            extendBook: "",
            ownerChat: "",
            deleteReservation: "",
            reserveBook: "",
            borrowBook: "",
        };
    }

}

