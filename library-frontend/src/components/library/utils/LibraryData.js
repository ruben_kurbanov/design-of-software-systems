import BookData from "./BookData";

export default class LibraryData {

    constructor(id = 0, borrowingPeriod = 0, visible = true, books = []) {
        this.id = id;
        this.borrowingPeriod = borrowingPeriod;
        this.visible = visible;
        this.books = books;
    }

    addBook(book = new BookData()) {
        this.books.push(book);
    }
}