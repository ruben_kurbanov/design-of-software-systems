export default class UserData {
    constructor(id = 0, email = "email", firstName = "name", surname = "surname", role = "role", photoLink = "", chatLink = "") {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.surname = surname;
        this.role = role;
        this.photoLink = photoLink;
        this.chatLink = chatLink;
    }
}