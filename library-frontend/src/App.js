import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Header from "./components/Header";
import Footer from "./components/Footer";

import Login from "./components/account/Login";
import Registration from "./components/account/Registration";
import Setting from "./components/account/Setting";
import Search from "./components/search/Search";
import LibraryMy from "./components/library/My";
import LibraryForeign from "./components/library/Foreign";
import Chat from "./components/account/Chat";

class App extends React.Component {
  addressBE = "http://localhost:8080/"

  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      token: ""
    };
    this.setToken = this.setToken.bind(this);
    this.resetToken = this.resetToken.bind(this);
    if (props.address) {
      this.addressBE = props.address;
    }
  }

  componentDidMount() {
    fetch(this.addressBE + "api/users/1")
        .then(res => res.json())
        .then(
            (result) => {
              this.setState({
                isLoaded: true,
                books: result.books
              });
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
        );
    this.setState({
      loaded: true
    });
  }

  setToken(newToken) {
    this.setState({
      token: newToken
    })
    console.log("New token set (" + newToken.substring(0, 8) + "..." + newToken.substring(newToken.length - 4) + ").")
  }

  resetToken() {
    this.setState({
      token: ""
    })
    console.log("Token reset. User was logged out.")
  }

  render(history) {
    return (
        <div className={"App"}>
          <BrowserRouter>
            <Header userLoggedOut={this.state.token === ""} resetToken={this.resetToken}/>

            {/*<p>token = {this.state.token}</p>*/}

            <Switch>
              <Route exact path="/login">
                <Login addressBE={this.addressBE} setToken={this.setToken}/>
              </Route>
              <Route exact path="/registration">
                <Registration addressBE={this.addressBE} token={this.state.token} setToken={this.setToken}/>
              </Route>
              <Route exact path="/setting">
                <Setting addressBE={this.addressBE} token={this.state.token}/>
              </Route>
              <Route exact path="/chat">
                <Chat addressBE={this.addressBE} token={this.state.token}/>
              </Route>
              <Route exact path="/library/my">
                <LibraryMy addressBE={this.addressBE} token={this.state.token}/>
              </Route>
              <Route exact path="/library/foreign/:userId">
                <LibraryForeign addressBE={this.addressBE} token={this.state.token}/>
              </Route>
              <Route exact path="/">
                <Search addressBE={this.addressBE} token={this.state.token} name={"NSS"}/>
              </Route>
            </Switch>

            <Footer />
          </BrowserRouter>
        </div>
    )
  }
}

export default App;
