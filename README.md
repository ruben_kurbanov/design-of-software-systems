# Základní info
Aplikace _Domácí knihovna_ je určena pro čtenáře a knihomily, kteří chtějí sdílet svojí domácí knihovnu s ostatními členy čtenářské komunity.

Po zaregistrování může uživatel vytvořit vlastní knihovnu, ve které může spravovat svoje knihy nebo může prohledávat knihovny ostatních uživatelů. V cizí knihovně si může uživatel půjčit nebo zarezervovat knihu – délku vypůjčení si stanovuje vlastník knihovny. Systém automaticky notifikuje uživatele o vypůjčených knihách a jejich lhůtách. Vlastní knihovnu může uživatel fragmentovat do sekcí.

## Téma semestrální práce:
- Název: Domácí knihovna
- s UI na frontendu
- Popis:
  - Aplikace pomocí, které jde sdílet obsah své domácí knihovny a půjčovat si knihy od ostatních uživatelů
  - V aplikaci si může uživatel vytvořit účet s vlastní knihovnu a do té si přidávat knihy, které následně může mazat nebo upravovat.
  - Z knihoven ostatních uživatelů si může půjčit knihu a poté ji vrátit.
  - Uživatelé mohou mezi sebou vytvářet chaty ve kterých mohou psát zprávy.
  - Aplikace vystavuje data přes REST rozhraní pomocí Rest controllerů.
  - K zabezpečení je použit Spring Security. K přístupu dat k databází se používá JPA a NamedQuery.

## Odkazy:
- Heroku deplyement server:
  - aplikace: https://library-fe.herokuapp.com/

## Instalace na localhostu se sdílenou DB
1.	naklonujte si repo
2.	zbuildujte BE část aplikace (adresář application/) mavenem - mvn clean package
3.	spusťte BE aplikace přes soubor _application/src/main/java/cz/cvut/kbss/ear/homeLibrary/App.java_
4.  spusťťte FE (varianta A) nebo přistupte k REST API pomocí postmana či jiného nástroje:  
  A.1.  přesuňte se do library-frontend/ a nainstalujte moduly pro node.js:  
        `npm install`  
        `npm install react-router-dom js-sha256`  
  A.2.  spusťte aplikaci pomocí `npm start`  
  B.1.	ve _doc/postman_ najdete testovací jsony do postmana

### Údaje k programům:
- **Frontend** (UI) se spouští přístupný na adrese: **localhost:3000**
  - nastavitelné v: **[library-frontend/package.json]**
  - adresa backendu je nastavitelná v [library-frontend/src/index.js]
    jako proměnná `var address`
- **Backend** se spouští přístupný na adrese: **localhost:8080**
  - má povolený přístup pouze pro localhost:3000
  - nastavitelné u jednotlivých metod tříd v: **[application/src/main/java/cz/cvut/kbss/ear/homeLibrary/api/]**
- **Databáze** je na školním serveru: slon.felk.cvut.cz:5432/db21_petrial3
  - nastavitelné v: **[application/src/main/resources/application.properties]**


# Popis aplikace
Účel aplikace je dát možnost lidem sdílet obsah své domácí knihovny a půjčovat si knihy od ostatních uživatelů.

Aplikace poskytuje rozhraní, přes které s ní může uživatel komunikovat.
V aplikaci si může uživatel vytvořit účet s vlastní knihovnu a do té si přidávat knihy které následně může mazat nebo upravovat.
Z knihoven ostatních uživatelů si může půjčit knihu a poté ji vrátit. Uživatelé mezi sebou vytvářet chaty ve kterých mohou psát zprávy.

Aplikace vystavuje data přes REST rozhraní pomocí Rest controllerů. K zabezpečení je použit Spring Security.
K přístupu dat k databází se používá JPA a NamedQuery.
Aplikace zachovává třívrstvou architekturu.

## Potřebný software
1.	Git
2.	Java 11
3.	Maven 3
4.	node.js (npm) 6
5.	Apache Tomcat
6.	Intelij IDEA nebo jiné IDE podle vaší preference
7.	Postman (pro komunikaci s aplikací) 

## Poznámky k implementaci
Není zcela hotový UI na frontende (adresář 'library-frontend/') a ke konci implementace jsme narazili na poměrně nutné
rozšíření REST API, aby seznamy knih v HTTP odpovědí měli základní údaje o vlastníkovi a šlo jej kontaktovat.  
Veškeré neuzavřené záležitosti mají stále otevřené issue.

## Druhy uživatelů a jejich funkce systému

**Nepřihlášený uživatel** si může prohlížet seznam knih co jsou dostupné ve veřejných knihovnách ostatních uživatelů. Pro další funkcionalitu si musí buď vytvořit účet nebo se přihlásit k již vytvořenému účtu. 
- prohlížení veřejných knihoven
- registrace

**Přihlášený uživatel** se může odhlásit a měnit uživatelské údaje. Dále může prohlížet knihovny ostatních uživatelů, vyhledávat knihy podle jejich údajů. Knihy z ostatních knihoven (ne vlastní) si může půjčovat, popřípadě rezervovat, pokud kniha není k dispozici (někdo jiný ji má půjčenou). Přihlášený uživatel si může vytvořit svojí vlastní knihovnu, ve které může přidávat, odebírat a upravovat knihy. Ke knize může uživatel přidat tagy, které zlepšují vyhledávaní a dělení na kategorie. Vlastní knihovnu může udělat veřejnou nebo privátní – nebude viditelná ostatními uživateli a knihy z ní nebudou vidět v seznamech. Pokud si někdo knihu vypůjčí dostane o tom vlastník knihy notifikaci s odkazem na chat s uživatelem, co o knihu má zájem (přes chat se pak domluví, jak si knihu předají). Přihlášení uživatelé mohou komunikovat přes chat, který si mohou vytvořit s jakýmkoliv přihlášeným uživatelem. Přihlášený uživatel může nahlásit jiného uživatele administrátorovi – v případě nevrácené knihy. Přihlášený uživatel si může zobrazit seznam zapůjčených knih, který funguje i jako historie jeho výpůjček. 
- prohlížení všech knihoven
- úprava uživatelského účtu
- CRUD vlastní knihovnu
- přidávání/odebírání knížek z vlastní knihovny
- upravovaní informací o knížkách 
- rezervovat/půjčovat knihy z knihoven ostatních uživatelů
- přidávat komentáře ke knihám


**Administrátor** může chatovat s uživateli přes chat a zablokovat funkce jednotlivým uživatelům, dokud se problém mezi nimi nevyřeší.

## Upřesnění rámce funkcionality
Systém nijak neručí za knihy co si uživatelé půjčují a je to na jejich vlastní nebezpečí, případné spory mohou být odeslány administrátorovi, který má právo zablokovat funkce účtu, pokud má dostatečné důkazy o vině uživatele. V systému na rozdíl od normální knihovny nenajdete žádné CD, noviny, elektronické knihy, audioknihy atd. co byste v normální knihovně hledali, slouží pouze pro tištěné tituly. 

